import {RouterModule, Routes} from '@angular/router';
// Import Guard
import {MaintenanceComponent} from './modules/shared/maintenance/maintenance.component';
import {ForbiddenComponent} from './modules/shared/forbidden/forbidden.component';
import {NotFoundComponent} from './modules/shared/not-found/not-found.component';
import {FrontComponent} from './modules/front/front.component';
import {DialogMediaComponent} from "./modules/front/components/dialogs/media/media.component";

// dialogs
import {DialogUnsubscribeComponent} from './modules/front/components/dialogs/unsubscribe/unsubscribe.component';

const rootRoutes: Routes = [
  {
    path: '',
    component: FrontComponent
  },
  {
    path: 'unsubscribe/:email_id',
    component: DialogUnsubscribeComponent,
    outlet: "dialog"
  },
  {
    path: 'maintenance',
    component: MaintenanceComponent,
  },
  {
    path: 'forbidden',
    component: ForbiddenComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  },
  {
    path: 'media/:media_id',
    component: DialogMediaComponent,
    outlet: 'dialog'
  }
];

export const AppRoutingModule = RouterModule.forRoot(rootRoutes);
