import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// Custom
import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from './modules/shared/shared.module';

import {FrontModule} from './modules/front/front.module';
import { ReadMorePipe } from './pipe/read-more.pipe';

// Dialogs
import {AppComponent} from './app.component';
import {DialogUnsubscribeComponent} from './modules/front/components/dialogs/unsubscribe/unsubscribe.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FrontModule,
    SharedModule,
  ],
  declarations: [
    AppComponent,
    DialogUnsubscribeComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
