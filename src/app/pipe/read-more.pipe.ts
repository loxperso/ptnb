import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readMore'
})
export class ReadMorePipe implements PipeTransform {

  content: string;
  transform(value?: any, max?: any): any {
    this.content = value;
    if (this.content.length >= max) {
      return this.content.substr(0, max) + '...';
    } else {
      return this.content;
    }
  }

}
