// Core
import {NgModule} from '@angular/core';

import {SharedModule} from '../shared/shared.module';

// Main
import {FrontRoutingModules} from './front-routing.module';
import {FrontComponent} from './front.component';

// Service API
import {BaseService} from '../../services/common/base.service';
import {MediaService} from '../../services/media.service';
import {ReferenceService} from '../../services/reference.service';
import {SubscribeService} from '../../services/subscribe.service';
import {LoginService} from '../../services/login.service';
import {ErrorService} from '../../services/common/error.service';
import {ContactService} from '../../services/contact.service';

// -->
// Custom components
// Media filters
import {MediaFilterSelectModule} from './components/media/filter/select/select.module';
import {MediaFilterTextModule} from './components/media/filter/text/text.module';

// Media icon
import {MediaIconModule} from './components/media/icon/icon.module';

// Media viewer
import {ReadMorePipe} from "../../pipe/read-more.pipe";
import {PdfViewerModule} from "ng2-pdf-viewer";
import { ViewerVideoComponent } from './components/media/viewer/video/video.component';
import { ViewerPdfComponent } from './components/media/viewer/pdf/pdf.component';
import { ViewerImageComponent } from './components/media/viewer/image/image.component';
import { ViewerComponent } from './components/media/viewer/viewer.component';

// Media Cards
import {CardsComponent} from './components/media/cards/cards.component';
import {MediaCardsCardModule} from './components/media/cards/card/card.module';
import {DialogMediaComponent} from './components/dialogs/media/media.component';

// Form
import {ContactFormComponent} from './components/form/contact/contact.component';
import {LoginFormComponent} from './components/form/login/login.component';
import {LogoutFormComponent} from './components/form/logout/logout.component';
import {ReadmoreComponent} from "./components/utils/readmore/readmore.component";

// Navigation
import {SidebarNavigationComponent} from './components/navigation/sidebar/sidebar.component';
import {TopbarNavigationComponent} from './components/navigation/topbar/topbar.component';
import { FooterComponent } from './components/navigation/footer/footer.component';

// Slider
import {MediaSliderComponent} from './components/media/slider/slider.component';
import {MediaSliderSlideDefaultModule} from './components/media/slider/slide/default/default.module';
import {MediaSliderSlideModule} from        './components/media/slider/slide/slide.module';

// --<

// domains
import {HomeComponent} from './domains/home/home.component';
import {SiteMapComponent} from './domains/sitemap/sitemap.component';
import {ContactComponent} from './domains/contact/contact.component';
import {TermsComponent} from './domains/terms/terms.component';
import {BrowseComponent} from './domains/browse/browse.component';
import {DomainUnsubscribeComponent} from './domains/unsubscribe/unsubscribe.component';

import {SubscribePopupComponent} from './components/dialogs/subscribe/subscribe.component';
import {LoginPopupComponent} from './components/dialogs/login/login.component';
import {SubscribeFormComponent} from './components/form/subscribe/subscribe.component';




@NgModule({
  imports: [
    SharedModule,
    FrontRoutingModules,
    // custom
    MediaSliderSlideDefaultModule,
    MediaSliderSlideModule,
    MediaFilterSelectModule,
    MediaFilterTextModule,
    MediaCardsCardModule,
    MediaIconModule,
    PdfViewerModule,
  ],
  declarations: [
    FrontComponent,
    // Custom
    // navigation
    FooterComponent,
    SidebarNavigationComponent,
    TopbarNavigationComponent,
    // Form
    ContactFormComponent,
    // Overlay
    SubscribePopupComponent,
    SubscribeFormComponent,
    LoginPopupComponent,
    LoginFormComponent,
    LogoutFormComponent,
    // Media slider
    MediaSliderComponent,
    CardsComponent,
    TermsComponent,
    DialogMediaComponent,
    // domains
    SiteMapComponent,
    HomeComponent,
    ContactComponent,
    BrowseComponent,
    DomainUnsubscribeComponent,
    // pipe
    ReadMorePipe,
    // Media viewer
    ViewerVideoComponent,
    ViewerPdfComponent,
    ViewerImageComponent,
    ReadmoreComponent,
    ViewerComponent,
  ],
  exports: [
    SharedModule,
  ],
  entryComponents: [
    SubscribePopupComponent,
    SubscribeFormComponent,
    LoginPopupComponent,
    LoginFormComponent,
    LogoutFormComponent,
    DomainUnsubscribeComponent,
    ViewerComponent,
  ],
})

export class FrontModule {
}
