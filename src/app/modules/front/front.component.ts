import {Component, OnInit, ViewChild} from '@angular/core';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';

// API service
import {MediaService} from '../../services/media.service';
import {ThemeService} from '../../services/theme.service';

// Scroll
import {ScrollService} from '../../services/scroll.service';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

declare const $: any;
@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.scss']
})
export class FrontComponent implements OnInit {

  @ViewChild(PerfectScrollbarComponent) scrollRef?: PerfectScrollbarComponent;
  isCardsLoaded = false;

  constructor(
    protected themeService: ThemeService,
    public scrollService: ScrollService,
    protected router: Router,
    protected route: ActivatedRoute,
  ) {
    this.scrollService.getIsHome().subscribe(val => {
      if (val === true) {
        this.scrollService.positionFooterHome();
      } else  {
        this.scrollService.resetHeightFooter();
      }
    });
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        if(!event.url.match(/browse/g)) {
          this.scrollRef.directiveRef.scrollToTop();
        }
        this.scrollService.positionFooterOnScrollHeight();

        setTimeout(() => {
          this.scrollService.positionFooterOnScrollHeight();
        }, 0);
      }
    });
  }

  ngOnInit() {
    // Using `document` or `window` is not a good practice
    // will break Angular Universal features
    // and escape the DOM virtualiser of Angular
    this.themeService.getData('favicon').subscribe(url => {
      const link = document.querySelector("link[rel*='icon']") || document.createElement('link');
      link['type'] = 'image/x-icon';
      link['rel'] = 'shortcut icon';
      link['href'] = url;
      document.getElementsByTagName('head')[0].appendChild(link);

      this.themeService.getData('color_primary').subscribe(color => this._setColor('--primary-color', color));
      this.themeService.getData('color_secondary').subscribe(color => this._setColor('--secondary-color', color));
      this.themeService.getData('color_tertiary').subscribe(color => this._setColor('--tertiary-color', color));
    });
  }

  private _setColor(prop, value) {
    document.getElementsByTagName('body')[0].style.setProperty(prop, value);
  }
}
