export const locale = {
  "filter": "Filtre",
  "newest": "Plus récent ",
  "oldest": "Moins récent",
  "all": "Tous",
  "no-result": "Aucun résultat"
};
