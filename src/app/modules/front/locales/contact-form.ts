export const locale = {
  "email-invalid": 'Email est invalide',
  "button-title": 'Envoyer',
  "message-ok": "Votre message a bien été envoyé",
  "message-error": "Le message n'a pas pu etre transmis"
};
