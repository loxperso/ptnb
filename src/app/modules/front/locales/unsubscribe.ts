export const locale = {
  "container-title": 'Abonnement aux notifications – Annulation',
  "message": "Nous vous confirmons, l’annulation de votre abonnement aux notifications lors de la publication de nouveaux contenus sur le site de sensibilisation au BIM du PTNB.",
  "close-button-label": "Fermer",
  "message-error": 'Impossible de vous desinscrire',
  "message-error-noemail": `Cette adresse email n'est pas enregistree`,
  "message-ok": 'Vous êtes bien inscrit à la newsletter',
};
