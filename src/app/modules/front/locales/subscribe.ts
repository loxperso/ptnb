export const locale = {
  "container-title": 'S\'abonner aux alertes',
  "email-required": 'Email est requis',
  "email-invalid": 'Email est invalide',
  "button-title": 'S\'abonner',
  'message-ok': 'Vous êtes bien inscrit à la newsletter',
  'message-error-email': 'Cette adresse email est déjà enregistrée',
  'message-email': 'Impossible de vous ajouter',
};
