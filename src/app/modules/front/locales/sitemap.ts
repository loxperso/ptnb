export const locale = {
  "container-title": 'PLAN DU SITE',
  "professions-label": 'Professions',
  "themes-label": 'Themes',
  "messages-label": 'Messages',
  "others-title": 'Autres pages',
};
