export const locale = {
  "footer-title": '© Copyright 2018 Plan de Transition Numérique du Bâtiment, Powered by Novalian®',
  "footer-contact": 'Contactez-nous',
  "footer-sitemap": 'Plan du site',
  "footer-terms": 'Mentions légales'
};
