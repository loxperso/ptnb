export const locale = {
  "container-title": 'Login',
  "email-required": 'Email est requis',
  "email-invalid": 'Email est invalide',
  "password-required": 'Password est requis',
  "button-title": 'Login'
};
