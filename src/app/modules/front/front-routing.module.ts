import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FrontComponent} from './front.component';


// Domains
import {HomeComponent} from './domains/home/home.component';
import {SiteMapComponent} from './domains/sitemap/sitemap.component';
import {ContactComponent} from './domains/contact/contact.component';
import {TermsComponent} from './domains/terms/terms.component';
import {BrowseComponent} from './domains/browse/browse.component';

const frontRoutes: Routes = [
  {
    path: '',
    component: FrontComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'contact',
        component: ContactComponent,
        children: []
      },
      {
        path: 'sitemap',
        component: SiteMapComponent,
      },
      {
        path: 'terms',
        component: TermsComponent,
        children: []
      },
      {
        path: 'browse/:profession_id/:theme_id/:message_id',
        component: BrowseComponent,
      },
    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(frontRoutes)
  ],
  exports: [RouterModule]
})
export class FrontRoutingModules {
}

