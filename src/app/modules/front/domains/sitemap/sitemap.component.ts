import { Component, OnInit } from '@angular/core';
import { locale } from '../../locales/sitemap';

// Rxjs
import {Subscription} from 'rxjs/Subscription';

// Layout
import {MediaChange, ObservableMedia} from '@angular/flex-layout';

import { locale as localeFooter } from '../../locales/footer';
import {
  trigger,
  transition,
  animate,
  style,
  query,
  stagger
} from '@angular/animations';

// API services
import {ReferenceService} from 'src/app/services/reference.service';
import {ScrollService} from 'src/app/services/scroll.service';

@Component({
  selector: 'app-front-domain-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss'],
  animations: [
    trigger('animate', [
      transition('* => *', [
        query(':enter', [
          style({ opacity: 0 }),
          stagger(100, [
            animate('.15s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
})
export class SiteMapComponent implements OnInit {

  locale = locale;
  localeFooter = localeFooter;

  public professions = null;
  public themes = null;
  public messages = null;

  public currentProfession;
  public currentTheme;

  watcher: Subscription;
  public isPhone: boolean = false;

  constructor(
    protected referenceService: ReferenceService,
    protected media: ObservableMedia,
    protected scrollService: ScrollService,
  ) {
    this.scrollService.setIsHome(false);
    this.watcher = media.subscribe((change: MediaChange) => {
      if (change.mqAlias == 'xs' || change.mqAlias == 'sm') {
         this.isPhone = true;
      } else {
         this.isPhone = false;
      }
    });
  }

  ngOnInit() {
    this.referenceService.getData('professions').subscribe(res => {
      this.professions = Array.from(res || []);
      this.select(this.professions[0]);

      setTimeout(() => {
        this.scrollService.positionFooterOnScrollHeight();
      }, 200);
    });
    this.referenceService.getData('themes').subscribe(res => {
      this.themes = Array.from(res || []);
      this.select(this.themes[0]);

      setTimeout(() => {
        this.scrollService.positionFooterOnScrollHeight();
      }, 200);
    });

    this.referenceService.getData('messages').subscribe(res => this.messages = Array.from(res || []));
  }

  public select(obj) {
    this.unSelectAll(obj.type);
    obj.active = true;

    if(obj.type === 'professions') {
      this.currentProfession = obj;
      this.unSelectAll('themes');
      (this.themes || [{}])[0].active = true;
    }

    if(obj.type === 'themes') {
      this.currentTheme = obj
    }

    return false;
  }

  public selectFromSelectbox(obj) {
    if(obj.value.type === 'themes') {
      this.currentTheme = obj.value
    }
    if(obj.type === 'professions') {
      this.currentProfession = obj.value
    }
  }

  private unSelectAll(type) {
    (this[type] || []).map(p => p.active = false);
  }
}
