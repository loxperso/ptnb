import { Component, OnInit, AfterViewInit } from '@angular/core';
import { locale } from '../../locales/contact'

import {ScrollService} from '../../../../services/scroll.service';

@Component({
  selector: 'app-front-domain-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements AfterViewInit {

  locale: object = locale;

  constructor(protected scrollService: ScrollService) {
    this.scrollService.setIsHome(false);
  }

  ngAfterViewInit() {
  }

}
