import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute }     from '@angular/router';

// Rxjs
import { Observable } from 'rxjs';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

// API services
import {ReferenceService} from 'src/app/services/reference.service';

import { locale } from '../../locales/browse';
import {ScrollService} from "../../../../services/scroll.service";

@Component({
  selector: 'app-front-domain-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})
export class BrowseComponent implements OnInit {

  locale = locale;
  public currentProfession;
  public currentTheme;
  public currentMessage;

  public isLoading = false;

  // Data set
  public types = [];
  public sort = [
    {id: 'desc', name: this.locale["newest"]},
    {id: 'asc', name: this.locale["oldest"]}
  ];
  public filters = {};
  public params = {};

  // Current selected values
  public currentType = null;
  public currentSort = null;
  public currentSearch = '';

  constructor(
    private referenceService: ReferenceService,
    private router: Router,
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private scrollService: ScrollService
  ) {
    this.scrollService.setIsHome(false);
  }

  ngOnInit() {

    // Retrieve the current profession, theme and message

    this.route.paramMap.subscribe(params => {

      this.params = {};

      this.referenceService.getData('professions', params.get('profession_id')).subscribe(res => {
        this.currentProfession = res;
        this.params['profession'] = res.id;
      });
      this.referenceService.getData('themes', params.get('theme_id')).subscribe(res => {
        this.currentTheme = res;
        this.params['theme'] = res.id;
      });
      this.referenceService.getData('messages', params.get('message_id')).subscribe(res => {
        this.currentMessage = res;
        this.params['message'] = res.id;
      });

    });

    // Rerieve the current filters

    this.route.queryParamMap.subscribe(filters => {

      // Since get type is a possible asynch operation,
      // bundle all filters in it to keep synchronised
      // the variables filters with other components
      this.referenceService.getData('types').subscribe(res => {

        // Re-init filters everythimes to trigger
        // NgOnChanges
        this.filters = {};

        this.types = [{ id: 'all', value: 'all', name: this.locale['all'] }].concat(res);
        this.currentType = this.types.filter(o => o.id === (filters.get('type') || 'all'))[0] || this.types[0];
        if(filters.get('type')) {
          this.filters['type'] = this.currentType.id;
        }

        // Handle sort
        this.currentSort = this.sort.filter(o => o.id === filters.get('sort'))[0] || this.sort[0];
        if(filters.get('sort')) {
          this.filters['sort'] = this.currentSort.id;
        }

        // Handle search
        this.currentSearch = filters.get('search');
        if(filters.get('search')) {
          this.filters['search'] = this.currentSearch;
        }
      });

    });
  }

  public selectType(type) {
    if(type.id === 'all') {
      delete this.filters['type'];
    } else {
      this.filters['type'] = type.id;
    }
    const queryParams = this.filters;
    this.router.navigate(['browse', this.params['profession'], this.params['theme'], this.params['message']], { queryParams });
  }

  public selectSort(sort) {
    this.filters['sort'] = sort.id;
    const queryParams = this.filters;
    this.router.navigate(['browse', this.params['profession'], this.params['theme'], this.params['message']], { queryParams });
  }

  public search(value) {
    this.currentSearch = value;

    if(this.currentSearch === '') {
      delete this.filters['search'];
    } else {
      this.filters['search'] = this.currentSearch;
    }
    const queryParams = this.filters;
    this.router.navigate(['browse', this.params['profession'], this.params['theme'], this.params['message']], { queryParams });
  }

  public hasLoaded(isLoading) {
    this.isLoading = isLoading;
    this.ref.detectChanges();
  }
}
