import { Component, OnInit, OnDestroy } from '@angular/core';
import { locale } from '../../locales/home';
declare const $: any;

@Component({
  selector: 'app-front-domain-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

   locale: object = locale;

  constructor() {}

  ngOnInit() {
    $('body').addClass('home');
  }

  ngOnDestroy() {
    $('body').removeClass('home');
  }
}
