import { Component, OnInit } from '@angular/core';
import { locale } from '../../locales/terms';
import {ScrollService} from "../../../../services/scroll.service";

@Component({
  selector: 'app-front-domain-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  locale: object = locale;

  constructor( private scrollService: ScrollService) {
    this.scrollService.setIsHome(false);
  }

  ngOnInit() {
  }

}
