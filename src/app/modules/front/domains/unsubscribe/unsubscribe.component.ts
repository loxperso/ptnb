import { Component, OnInit, Inject } from '@angular/core';
import { HttpParams } from '@angular/common/http';

// Material
import { MatDialog, MAT_DIALOG_DATA } from "@angular/material";
import {MatSnackBar} from '@angular/material';

// Service API
import { UnsubscribeService } from 'src/app/services/unsubscribe.service';

import { locale } from '../../locales/unsubscribe';

@Component({
  selector: 'app-front-domain-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss'],
  providers: [UnsubscribeService],
})
export class DomainUnsubscribeComponent implements OnInit {

  locale = locale;

  constructor(
    protected snackBar: MatSnackBar,
    protected service: UnsubscribeService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    let email;
    email['email'] = this.data.email;
    this.service.postUnsubscribe(email).subscribe(this.onSuccess, this.onFail);
  }

  private onSuccess = () => {}

  private onFail = e => {
    const err = e.error && e.error.error === 'not_subscribed' ? this.locale['message-error-noemail'] : this.locale['message-error']
    this.snackBar.open(err, '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['error-snackbar']});
  }
}
