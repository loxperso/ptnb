import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { locale } from '../../../locales/subscribe';
import {SubscribeService} from '../../../../../services/subscribe.service';
import {MatSnackBar} from '@angular/material';
import {ScrollService} from "../../../../../services/scroll.service";

@Component({
  selector: 'app-front-form-subscribe',
  templateUrl: '../../form/subscribe/subscribe.component.html',
  styleUrls: ['../../form/subscribe/subscribe.component.scss'],
  providers: [SubscribeService]
})

export class SubscribeFormComponent {

  subscribeForm: FormGroup;
  locale: object = locale;

  // Outputs
  @Output() subscribed: EventEmitter<any> = new EventEmitter();

  constructor(
    protected fb: FormBuilder,
    protected service: SubscribeService,
    public snackBar: MatSnackBar,
    private scrollService: ScrollService

  ) {
    this.subscribeForm = this.fb.group({
      email: ["", Validators.compose([Validators.required,
         Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    });
  }

  public get isInvalid() {
    return (
      this.subscribeForm.controls.email.invalid
    );
  }

  public subscribe() {
    let value = this.subscribeForm.value;
    this.service.postSubscribe(value).subscribe(this.onSuccess, this.onFail);

  }

  onSuccess = () => {
    this.snackBar.open(this.locale['message-ok'], '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['valid-snackbar']});
    this.subscribed.emit();
  }

  onFail = e => {
    const err = e.error && e.error.error === 'already_subscribed' ? this.locale['message-error-email'] : this.locale['message-error-email'];
    this.snackBar.open(err, '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['error-snackbar']});
  }

  topBarCloseDialog() {
    this.scrollService.setClickCloseTopbar(true);
  }

}
