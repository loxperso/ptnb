import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {LoginService} from '../../../../../services/login.service';
import {MatSnackBar} from '@angular/material';
import { locale } from '../../../locales/login';
import {ScrollService} from "../../../../../services/scroll.service";



@Component({
  selector: 'app-front-form-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})

export class LoginFormComponent {

  locale: object = locale;
  loginForm: FormGroup;

  // Outputs
  @Output() logged: EventEmitter<any> = new EventEmitter();

  constructor(
    protected service: LoginService,
    public snackBar: MatSnackBar,
    protected fb: FormBuilder,
    private scrollService: ScrollService
  ) {
          this.loginForm = this.fb.group({
            password: ["", Validators.required],
            email: ["", Validators.compose([Validators.required,
               Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
          });
    }

    public get isInvalid() {
      return (
        this.loginForm.controls.email.invalid ||
        this.loginForm.controls.password.invalid
      );
    }

    public login() {
      let value = this.loginForm.value;
      this.service.postLogin(value).subscribe(this.onSuccess, this.onFail);
    }

    public onSuccess = res => {
      this.snackBar.open('Vous êtes connecté', '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['valid-snackbar']});
      this.logged.emit();
    }

    public onFail = e => {
      this.snackBar.open('Votre adresse et/ou mot de passe sont incorrects', '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['error-snackbar']});
    }

    topBarCloseDialog() {
      this.scrollService.setClickCloseTopbar(true);
    }
}
