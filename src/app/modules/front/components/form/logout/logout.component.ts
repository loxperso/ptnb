import { Component, OnInit, Inject, ComponentRef, Output, EventEmitter } from '@angular/core';

// material
import {MatSnackBar} from '@angular/material';

// Locale
import {locale} from 'src/app/modules/front/locales/logout';
import {ScrollService} from "../../../../../services/scroll.service";

@Component({
  selector: 'app-front-form-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutFormComponent implements OnInit {

  @Output() loggedout: EventEmitter<any> = new EventEmitter();;

  locale = locale;

  constructor(protected snackBar: MatSnackBar,
              private scrollService: ScrollService) {}

  ngOnInit() {}

  public logout() {
    // TODO Implement Logout accordingly to your local storage strategy
    this.snackBar.open(this.locale['message-ok'], '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['valid-snackbar']});
    this.loggedout.emit();
  }

  topBarCloseDialog() {
    this.scrollService.setClickCloseTopbar(true);
  }
}
