import { Component, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {ContactService} from '../../../../../services/contact.service';
import {MatSnackBar} from '@angular/material';
import { locale } from '../../../locales/contact-form';
import {ScrollService} from "../../../../../services/scroll.service";

@Component({
  selector: 'app-front-form-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [ContactService]
})
export class ContactFormComponent {

  locale = locale;
  contactForm: FormGroup;

  constructor(
    protected fb: FormBuilder,
    public snackBar: MatSnackBar,
    protected service: ContactService,
    private scroll: ScrollService
  ) {
    this.contactForm = this.fb.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      email: ["", Validators.compose([Validators.required,
  	     Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
       ])],
      message: ["", Validators.required]
    });
  }

  public get isInvalid() {
    return (
      this.contactForm.controls.first_name.invalid ||
      this.contactForm.controls.last_name.invalid ||
      this.contactForm.controls.email.invalid ||
      this.contactForm.controls.message.invalid
    );
  }

  public contact() {
    let value = this.contactForm.value;
    this.service.postContact(value).subscribe(this.onSuccess, this.onFail);
  }

  private onSuccess = () => {
    this.snackBar.open(this.locale["message-ok"], '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['valid-snackbar']});
    this.contactForm.reset();
  }

  private onFail = () => {
    this.snackBar.open(this.locale["message-error"], '', {duration: 2500, verticalPosition: 'top', horizontalPosition: 'left', panelClass: ['error-snackbar']});
  }

}
