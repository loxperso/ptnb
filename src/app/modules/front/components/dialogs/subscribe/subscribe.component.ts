import { Component, OnInit, ComponentRef } from '@angular/core';

import {OverlayModule, Overlay, OverlayConfig} from '@angular/cdk/overlay';
import {SubscribeFormComponent} from '../../form/subscribe/subscribe.component';

import { locale } from 'src/app/modules/front/locales/subscribe';

import {
  QueryList,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';

import {
  ComponentPortal,
} from '@angular/cdk/portal';
import {ScrollService} from "../../../../../services/scroll.service";

@Component({
  selector: 'app-front-dialogs-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss'],
})
export class SubscribePopupComponent {

  rightPosition: number = 45;
  topPosition: number = 56;

  locale = locale;

  constructor(
    public overlay: Overlay,
    public viewContainerRef: ViewContainerRef,
    private scrollService: ScrollService,
  ) {}

  openSubscribePanel() {
    let config = new OverlayConfig();

    config.positionStrategy = this.overlay.position()
        .global()
        .right(`${this.rightPosition}px`)
        .top(`${this.topPosition}px`);

    config.hasBackdrop = true;

    let overlayRef = this.overlay.create(config);

    this.scrollService.getClickCloseTopbar().subscribe( res => {
      if (res) {
        overlayRef.dispose();
        this.scrollService.setClickCloseTopbar(false);
      }
    });

    overlayRef.backdropClick().subscribe( () => {
      overlayRef.dispose();
    });

    const portal = new ComponentPortal(SubscribeFormComponent, this.viewContainerRef)
    const compRef: ComponentRef<SubscribeFormComponent> = overlayRef.attach(portal);

    compRef.instance.subscribed.subscribe(() => overlayRef.dispose());
  }
}
