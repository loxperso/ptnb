import { Component, OnInit, Inject, ComponentRef } from '@angular/core';

import {OverlayModule, Overlay, OverlayConfig} from '@angular/cdk/overlay';
import {LoginFormComponent} from '../../form/login/login.component';
import {LogoutFormComponent} from '../../form/logout/logout.component';

import {
  QueryList,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';

import {
  ComponentPortal,
} from '@angular/cdk/portal';
import {ScrollService} from "../../../../../services/scroll.service";

@Component({
  selector: 'app-front-dialogs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginPopupComponent implements OnInit {

  isUserLoggedIn:boolean = false;

  rightPosition: number = 5;
  topPosition: number = 56;

  constructor(
    public overlay: Overlay,
    public viewContainerRef: ViewContainerRef,
    private scrollService: ScrollService
  ) {}

  ngOnInit() {
    this.isUserLoggedIn = false;
  }

  openPanel() {
    let config = new OverlayConfig();

    config.positionStrategy = this.overlay.position()
        .global()
        .right(`${this.rightPosition}px`)
        .top(`${this.topPosition}px`);

    config.hasBackdrop = true;

    let overlayRef = this.overlay.create(config);

    overlayRef.backdropClick().subscribe(() => {
      overlayRef.dispose();
    });
    this.scrollService.getClickCloseTopbar().subscribe( res => {
      if (res) {
        overlayRef.dispose();
        this.scrollService.setClickCloseTopbar(false);
      }
    });

    // Logout
    if(this.isUserLoggedIn) {
      const portal = new ComponentPortal(LogoutFormComponent, this.viewContainerRef);
      const compRef: ComponentRef<LogoutFormComponent> = overlayRef.attach(portal);
      compRef.instance.loggedout.subscribe(() => overlayRef.dispose());
    }
    // Login
    else {
      const portal = new ComponentPortal(LoginFormComponent, this.viewContainerRef);
      const compRef: ComponentRef<LoginFormComponent> = overlayRef.attach(portal);
      compRef.instance.logged.subscribe(() => overlayRef.dispose());
    }

  }
}
