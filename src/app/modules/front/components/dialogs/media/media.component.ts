import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

// Material
import {MatDialog} from "@angular/material";
import {MediaService} from "../../../../../services/media.service";

// Custom
import {ViewerComponent} from "../../media/viewer/viewer.component";


@Component({
  selector: 'app-front-dialog-media',
  template: ''
})
export class DialogMediaComponent {

  constructor(
    private ref: ChangeDetectorRef,
    private activateRoute: ActivatedRoute,
    private router: Router,
    private matDialog: MatDialog,
    private mediaService: MediaService
  ) {
    this.activateRoute.paramMap.subscribe( res => {
      if (res.get('media_id')) {
        const config = {
          data: { media_id: res.get('media_id') },
          width: '100%',
          maxWidth: '100%',
          disableClose: false,
          backdropClass: 'ptnb-viewer',
          panelClass: 'ptnb-viewer',
          autoFocus: false,
        };
        const dialogRef = this.matDialog.open(ViewerComponent, config);

        dialogRef.afterClosed().subscribe(detail => {
          this.router.navigate([{ outlets: { dialog: null } }], { queryParamsHandling: 'preserve' });
        });

      }
    });
  }

}
