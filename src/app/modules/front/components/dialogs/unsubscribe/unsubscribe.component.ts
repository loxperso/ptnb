import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute }     from '@angular/router';

// Material
import { MatDialog } from "@angular/material";

// Custom
import { DomainUnsubscribeComponent } from 'src/app/modules/front/domains/unsubscribe/unsubscribe.component';

@Component({
  selector: 'app-front-dialogs-unsubscribe',
  template: '',
  styleUrls: []
})
export class DialogUnsubscribeComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
  ) {
    this.route.paramMap.subscribe(params => {
      const config = {
        width: '650px',
        position: { top: '54px' },
        data: { email: params.get('email_id') }
      };

      const dialogRef = this.dialog.open(DomainUnsubscribeComponent, config);
      dialogRef.afterClosed().subscribe(this.back);
    });
  }

  ngOnInit() {
  }

  private back = () => {
    this.router.navigate(['/', { outlets: { dialog: null } }], { queryParamsHandling: 'preserve' });
  }
}
