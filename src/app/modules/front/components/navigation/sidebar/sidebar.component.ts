import {Component, OnInit, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Router, NavigationStart} from '@angular/router';

// Rxjs
import {Subscription} from 'rxjs/Subscription';

// Layout
import {MediaChange, ObservableMedia} from '@angular/flex-layout';

// API Service
import {ReferenceService} from 'src/app/services/reference.service';


import { locale as localeFooter } from 'src/app/modules/front/locales/footer';

@Component({
  selector: 'app-front-navigation-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarNavigationComponent implements OnInit {

  // Outputs
  @Output() navigateOnPhone: EventEmitter<any> = new EventEmitter();

  public professions;
  public themes;
  public messages;
  public userIsLoggedIn: boolean = true;

  localeFooter = localeFooter;

  watcher: Subscription;
  public isPhone: boolean = false;

  constructor(
    private referenceService: ReferenceService,
    protected media: ObservableMedia,
    protected router: Router,
  ) {
    this.watcher = media.subscribe((change: MediaChange) => {
      if (change.mqAlias == 'xs' || change.mqAlias == 'sm') {
         this.isPhone = true;
      } else {
         this.isPhone = false;
      }
    });

    this.router.events.subscribe(event => {
      if(this.isPhone && event instanceof NavigationStart) {
        this.navigateOnPhone.emit();
      }
    })
  }

  ngOnDestroy() {
    this.watcher.unsubscribe();
  }

  ngOnInit() {
    this.buildTree();
  }

  getOrdered = (a, b) => {
    if(a.display_order < b.display_order) return -1;
    if(a.display_order > b.display_order) return 1;
    return 0;
  }

  private buildTree() {
    this.referenceService.getData('professions').subscribe(res => this.professions = res.sort(this.getOrdered) || []);
    this.referenceService.getData('themes').subscribe(res => this.themes = res.sort(this.getOrdered) || []);
    this.referenceService.getData('messages').subscribe(res => this.messages = res || []);
  }

  public expand(profession, theme = null) {
    this.professions.map(p => {
      if(p.id !== profession.id) {
        p.isExpanded = false;
      }
    });

    if(theme) {
      theme.isExpanded = !theme.isExpanded ? true : false;
      this.themes.map(t => {
        if(t.id !== theme.id) {
          t.isExpanded = false;
        }
      });
    } else {
      this.themes.map(t => t.isExpanded = false);
      profession.isExpanded = !profession.isExpanded ? true : false;
    }

    return false;
  }

  public navigateTo(location) {
    // Close the sidebar on click link for phone
    if(this.isPhone) {
      this.navigateOnPhone.emit(location);
    }
    this.router.navigate(location);
    return false;
  }
}
