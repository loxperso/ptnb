import { Component, AfterViewInit } from '@angular/core';

// Rxjs
import {Subscription} from 'rxjs/Subscription';

// Layout
import {MediaChange, ObservableMedia} from '@angular/flex-layout';

// Scroll
import {ScrollService} from 'src/app/services/scroll.service';

import { locale } from '../../../locales/footer';

@Component({
  selector: 'app-front-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements AfterViewInit {

  locale: object = locale;

  public isData: boolean = false;

  public isPhone: boolean = false;
  private watcher: Subscription;

  constructor(protected media: ObservableMedia, protected scrollService: ScrollService,) {
    this.watcher = media.subscribe((change: MediaChange) => {
      if (change.mqAlias == 'sm' || change.mqAlias == 'xs') {
         this.isPhone = true;
      } else {
         this.isPhone = false;
      }
    });
  }

  ngAfterViewInit() {
    this.scrollService.positionFooterOnScrollHeight();
  }

}
