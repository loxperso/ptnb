import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';

// Rxjs
import {Subscription} from 'rxjs/Subscription';

// Layout
import {MediaChange, ObservableMedia} from '@angular/flex-layout';

// API Service
import {ThemeService} from 'src/app/services/theme.service';

/** @title Responsive mainnav */
@Component({
  selector: 'app-front-navigation-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarNavigationComponent implements OnInit, OnDestroy {

  @ViewChild('snav') snav: any;

  public isOpen: boolean = false;
  public logoUrl:string = '';
  public userIsLoggedIn: boolean = false;
  public isPhone: boolean = false;

  private watcher: Subscription;

  constructor(
    protected themeService: ThemeService,
    protected media: ObservableMedia,
  ) {
    this.watcher = media.subscribe((change: MediaChange) => {
      if (change.mqAlias == 'sm' || change.mqAlias == 'xs') {
         this.isPhone = true;
      } else {
         this.isPhone = false;
      }
    });
  }

  ngOnDestroy() {
    this.watcher.unsubscribe();
  }

  ngOnInit() {
    this.themeService.getData('logo').subscribe(logo => this.logoUrl = logo);
  }

  closeSidebar() {
    this.snav.close();
  }
  public toggle() {
    if(this.isOpen) {
      this.snav.close();
    } else {
      this.snav.open();
    }

    this.isOpen = this.snav.opened;
  }

  public closeOnPhone() {
    if(this.isOpen) {
      this.snav.close();
    }
  }

  public onClose() {
    this.isOpen = false;
    this.snav.close();
  }
}
