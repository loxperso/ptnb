import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

// Metarial
import {MatIconModule} from '@angular/material';

// custom
import {MediaIconComponent} from './icon.component';

@NgModule({
  imports: [ CommonModule, MatIconModule ],
  declarations: [ MediaIconComponent],
  exports: [ MediaIconComponent ],
})

export class MediaIconModule {
}
