// Angular
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-front-media-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class MediaIconComponent implements OnInit {

  @Input() media;

  constructor() {
  };

  ngOnInit() {
  }

}
