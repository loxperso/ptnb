// Angular
import {
  Component,
  OnDestroy,
  OnChanges,
  Input,
  Output,
  ChangeDetectorRef,
  EventEmitter
} from "@angular/core";

import {
  trigger,
  transition,
  animate,
  style,
  query,
  stagger
} from '@angular/animations';
import {HttpParams} from '@angular/common/http';
import {ActivatedRoute, Router} from "@angular/router";

// Material
import {MatDialog, MatSnackBar} from "@angular/material";
import {MatSelectModule} from '@angular/material/select';

// Custom
import {MediaService} from 'src/app/services/media.service';

// Scroll
import {ScrollService} from 'src/app/services/scroll.service';

import {locale} from 'src/app/modules/front/locales/browse';


@Component({
  selector: 'app-front-media-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
  animations: [
    trigger('staggerAnimation', [
      transition('* => *', [
        query(':enter', [
          style({ opacity: 0 }),
          stagger(200, [
            animate('.3s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
})

export class CardsComponent implements OnChanges, OnDestroy {

  @Output() loading:EventEmitter<any> = new EventEmitter();

  @Input() params;
  @Input() filters;

  // Outputs

  media: any = null;
  isLoadingData = true;

  titleFilter: string = '';
  sortSelected: string;
  currentPage: number = 1;
  currentLimit: string = '6';

  totalPage: number = 1;
  
  public locale = locale;

  private subscription;

  public displayNoResult: boolean = false;

  constructor(
    protected mediaService: MediaService,
    protected scrollService: ScrollService,
    public snackBar: MatSnackBar,
    private matDialig: MatDialog,
    private ref: ChangeDetectorRef,
    private activateRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.subscription = this.scrollService.scroll().subscribe(res => {
      this.loadNext();
    });
  }
  ngOnChanges(changes) {
    this.media = null;
    this.currentPage = 1;
    this.displayNoResult = false;
    this.getListMedia(true);
  }
  ngOnDestroy() {
    this.media = [];
    this.ref.detach();
    this.subscription.unsubscribe();
  }

  getListMedia(reload = false) {
    // Avoid making bad requests
    if(!this.params.theme || !this.params.profession || !this.params.message) {
      return ;
    }

    // Boilerplate on loading and no result display
    this.isLoadingData = this.currentPage < this.totalPage;
    this.displayNoResult = false;

    let params = new HttpParams()
      .set('theme', this.params.theme)
      .set('profession', this.params.profession)
      .set('message', this.params.message)
      .set('size', this.currentLimit + '')
      .set('page', this.currentPage + '');
    // Set Sort filter
    if (this.filters.sort) {
      params = params.append('sortOrder', this.filters.sort);
      params = params.append('sortBy', 'date');
    }

    // Set Type filter
    if (this.filters.type) {
      params = params.append('types', `["${this.filters.type}"]`);
    }

    // Set Search filter
    params = params.append('search', this.filters.search || '');

    this.loading.emit(true);

    // Request media
    this.mediaService.getList(params).subscribe(media => {

      this.totalPage = media['meta']['pagination']['total_pages'];

      // Add new result to the current data
      if(!reload) {
        this.media = this.media ? this.media.concat(media['data']) : media['data'];
      } else {
        this.media = media['data'];
      }

      setTimeout(() => {
        // Avoid displaying no-rsult during the render
        if(this.media.length === 0) {
          this.displayNoResult = true;
        }

        this.scrollService.positionFooterOnScrollHeight();
        this.loading.emit(false);
        this.isLoadingData = false;

        // Tell Angular to update the DROM
        this.ref.detectChanges();

      }, 300);
    });
  }

  loadNext() {
    if(this.currentPage < this.totalPage) {
      this.currentPage++;
      this.getListMedia();
      this.ref.detectChanges();
    } else {
      this.isLoadingData = false;
    }
  }
}
