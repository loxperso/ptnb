// Angular
import {Component, Input} from "@angular/core";
import {DomSanitizer} from '@angular/platform-browser';

// Custom
import {MediaSliderSlideComponent} from './../../slider/slide/slide.component';

@Component({
  selector: 'app-front-media-cards-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})

export class MediaCardsCardComponent extends MediaSliderSlideComponent {

  @Input() media;

  constructor(protected domSanitizer: DomSanitizer) {
    super(domSanitizer);
  }
}
