// Dependencies
import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

// Material
import { MatCardModule }          from '@angular/material/card';
import { MatIconModule }          from '@angular/material/icon';

// Custom
import { MediaCardsCardComponent } from './card.component';
import { MediaIconModule } from './../../icon/icon.module';
import {RouterModule} from "@angular/router";


@NgModule({
  imports: [
    CommonModule,
    // Material
    MatCardModule,
    MatIconModule,
    MediaIconModule,
    RouterModule
  ],
  declarations: [ MediaCardsCardComponent ],

  exports: [
    // Material
    MatCardModule,
    MatIconModule,
    MediaIconModule,
    MediaCardsCardComponent,
  ]
})

export class MediaCardsCardModule {}
