// Dependencies
import { Component, OnChanges, Output, Input, EventEmitter } from '@angular/core';

// Component decoraction
@Component({
  selector: 'app-front-media-filter-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})

export class MediaFilterSelectComponent implements OnChanges {

  // Outputs
  @Output() select: EventEmitter<any> = new EventEmitter();

  // Inputs
  @Input() value = null;
  @Input() data = null;

  public currentValue = null;

  constructor() {
  }


  ngOnChanges(changes) {
    this.currentValue = this.value;
  }


  public selectValue(event){
    this.value = this.displayWith(event.value);
    this.currentValue = event.value;
    this.select.emit(this.currentValue);
  }


  public displayWith = (option) => {
    return option ? option.name : '';
  };

}
