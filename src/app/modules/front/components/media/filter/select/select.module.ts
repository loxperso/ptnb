// Dependencies
import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

// Material
import { MatSelectModule }          from '@angular/material/select';


// Custom
import { MediaFilterSelectComponent } from './select.component';


@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
  ],
  declarations: [ MediaFilterSelectComponent ],

  exports: [
    MediaFilterSelectComponent,
    MatSelectModule,
  ]
})

export class MediaFilterSelectModule {}
