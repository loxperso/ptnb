// Dependencies
import { Component, OnChanges, Output, Input, EventEmitter } from '@angular/core';

// Component decoraction
@Component({
  selector: 'app-front-media-filter-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})

export class MediaFilterTextComponent implements OnChanges {


  private _timer;

  // Outputs
  @Output() search: EventEmitter<any> = new EventEmitter();

  // Inputs
  @Input() value = null;
  @Input() isLoading = false;

  constructor() {
  }


  ngOnChanges(changes) {
  }


  public onInput(event){
    clearTimeout(this._timer);
    this._timer = setTimeout(() => this._doSearch(event.target.value), 300);
  }

  private _doSearch(value) {
    this.search.emit(value)
  }

}
