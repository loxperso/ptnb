// Dependencies
import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

// Material
import { MatIconModule }          from '@angular/material/icon';
import { MatButtonModule }          from '@angular/material/button';
import { MatProgressSpinnerModule }          from '@angular/material/progress-spinner';


// Custom
import { MediaFilterTextComponent } from './text.component';


@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
  declarations: [ MediaFilterTextComponent ],

  exports: [
    MediaFilterTextComponent,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ]
})

export class MediaFilterTextModule {}
