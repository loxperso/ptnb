import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';

// Custom
import {MediaSliderSlideComponent} from './slide.component';


@NgModule({
  imports: [ CommonModule, RouterModule ],
  declarations: [ MediaSliderSlideComponent],
  exports: [ MediaSliderSlideComponent ],
})

export class MediaSliderSlideModule {
}
