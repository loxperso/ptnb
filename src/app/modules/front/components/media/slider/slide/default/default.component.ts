// Angular
import {Component, OnInit, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

// custom
import {MediaSliderSlideComponent} from './../slide.component';

@Component({
  selector: 'app-front-media-slider-slide-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss'],
})
export class MediaSliderSlideDefaultComponent extends MediaSliderSlideComponent implements OnInit {

  constructor(protected domSanitizer: DomSanitizer) {
    super(domSanitizer);
  };

  ngOnInit() {
  }

}
