import {Component, OnInit, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-front-media-slider-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
})
export class MediaSliderSlideComponent implements OnInit {

  @Input() item;

  constructor(protected domSanitizer: DomSanitizer) {}

  ngOnInit() {
  }

  public buildDescription(item) {
    let wordAmount = 5, rowAmount = 3, totalWord = item.description.split(' ').length,
      _rows = Math.ceil(totalWord/wordAmount),
      rows = _rows > rowAmount ? rowAmount : _rows,
      output = [],
      ii = 0;
    for(let i = 0; i < rows; i++ ) {
      output[i] = ''
      for(ii; ii < wordAmount * (i+1); ii++ ) {
        output[i] += ` ${item.description.split(' ')[ii] || ''}`;
        if(ii+1 === rowAmount*wordAmount && i+1 === rowAmount && totalWord > rowAmount*wordAmount) {
          output[i] += ' ...';
        }
      }
      output[i].trim();
    }
    return output;
  }

  public backgroundImage(url) {
    const output = `background-image: url(${url})`;
    return this.domSanitizer.bypassSecurityTrustStyle(output);
  }
}
