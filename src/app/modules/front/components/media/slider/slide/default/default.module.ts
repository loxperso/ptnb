import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';

// Metarial
import {MatIconModule} from '@angular/material';

// custom
import {MediaSliderSlideDefaultComponent} from './default.component';

@NgModule({
  imports: [ CommonModule, MatIconModule, RouterModule ],
  declarations: [ MediaSliderSlideDefaultComponent],
  exports: [ MediaSliderSlideDefaultComponent ],
})

export class MediaSliderSlideDefaultModule {
}
