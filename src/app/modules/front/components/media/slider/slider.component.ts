import {Component, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {
  trigger,
  transition,
  animate,
  style,
  query,
  stagger
} from '@angular/animations';
import {MediaService} from 'src/app/services/media.service';
import {ScrollService} from "../../../../../services/scroll.service";

@Component({
  selector: 'app-front-media-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition(":enter", [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
    ])
  ]
})
export class MediaSliderComponent implements OnInit {

  @ViewChild('slickModal') slickModal: any;

  public slides = null;

  public slideConfig = {
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 3,
    dots: true,
    slidesToScroll: 1,
    infinite: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          arrows: false
        }
      },
    ]
  };

  constructor(private mediaService: MediaService,
              protected domSanitizer: DomSanitizer,
              private scrollService: ScrollService,
  ) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.slides = null;
    this.mediaService.getLatest().subscribe(res => {
      setTimeout(() => {
        this.slides = res;
      }, 1000)
    });
  }

  positionFooter() {
    this.scrollService.setIsHome(true);
  }
}
