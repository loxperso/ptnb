import {ChangeDetectorRef, Component, Inject, OnInit, OnDestroy} from "@angular/core";
import {HttpParams} from "@angular/common/http";

// Material
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

// Contrib
import videojs from 'video.js';

// API service
import {ReferenceService} from "../../../../../services/reference.service";
import {MediaService} from "../../../../../services/media.service";

@Component({
  selector: 'app-views',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent {

  type = null;
  format = null;
  media: any;

  constructor(
    private mediaService: MediaService,
    private ref: ChangeDetectorRef,
    public dialogRef: MatDialogRef<ViewerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.mediaService.getById(this.data.media_id).subscribe(result => {

      this.media = result;

      this.type = this.media.format.toLowerCase() === 'pdf' ? 'pdf' : null;

      if (!this.type) {
        this.type = this.media.type === 'VI' ? 'video' : 'image';
      }
    });
  }

  public download(media) {
    console.log(this.mediaService.getDownloadURL(media.id));
    window.open(this.mediaService.getDownloadURL(media.id), '_blank');
  }

}
