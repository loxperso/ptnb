import {Component, OnInit, Input } from "@angular/core";

@Component({
  selector: 'app-viewer-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ViewerImageComponent implements OnInit {

  @Input() dataImage;
  constructor() {
  }

  ngOnInit() {
  }

}
