import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import videojs from 'video.js';

@Component({
  selector: 'app-viewer-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class ViewerVideoComponent implements AfterViewInit, OnDestroy {

  @Input() media;
  player;

  constructor() { }

  ngAfterViewInit() {
    this.player = videojs('ptnb-player', {
      controlBar: {
        volumePanel: { inline: false },
      },
      textTrackSettings: false
    });
  }

  ngOnDestroy() {
    this.player.dispose();
  }

}
