import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerVideoComponent } from './video.component';

describe('ViewerVideoComponent', () => {
  let component: ViewerVideoComponent;
  let fixture: ComponentFixture<ViewerVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
