import { Component, OnInit, Input } from '@angular/core';

import { locale } from 'src/app/modules/front/locales/viewer';

declare const $: any;
@Component({
  selector: 'app-viewer-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class ViewerPdfComponent implements OnInit {

  @Input() media;

  isLoaded: boolean = false;
  page: number = 1;
  totalPages: number|null;

  locale = locale;

  constructor() { }

  ngOnInit() {
    this.isLoaded = false;
    this.page = 1;
    this.totalPages = null;
  }

  afterLoadComplete(pdf: any) {
    this.totalPages = pdf.numPages;
    this.isLoaded = true;
    // if (this.iOS()) {
    //   $('.pdf-view > .ng2-pdf-viewer-container').css('overflow-y', 'auto');
    // }
  }

  nextPage() {
    this.page++;
  }

  prevPage() {
    this.page--;
  }

  iOS() {
    const iDevices = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ];
    if (!!navigator.platform) {
      while (iDevices.length) {
        if (navigator.platform === iDevices.pop()) {
          return true;
        }
      }
    }
    return false;
  }

}
