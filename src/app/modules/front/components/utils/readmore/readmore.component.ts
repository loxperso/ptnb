import {Component, OnInit, Input, ChangeDetectorRef} from "@angular/core";

@Component({
  selector: 'app-readmore',
  templateUrl: './readmore.component.html',
  styleUrls: ['./readmore.component.scss']
})
export class ReadmoreComponent implements OnInit {

  @Input('data') desc;
  @Input() maxLenght = 300;

  isCheckLenght = false;
  isChecked: boolean;

  constructor( private ref: ChangeDetectorRef ) { }

  ngOnInit() {
    this.isChecked = this.desc.description.length >= this.maxLenght ? true : false;
  }

  show_more() {
    this.isCheckLenght = !this.isCheckLenght;
    this.ref.detectChanges();
    return false;
  }

}
