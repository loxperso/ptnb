// Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OverlayModule} from '@angular/cdk/overlay';
import {A11yModule} from '@angular/cdk/a11y';
// Default state
import {ForbiddenComponent} from './forbidden/forbidden.component';
import {MaintenanceComponent} from './maintenance/maintenance.component';
import {NotFoundComponent} from './not-found/not-found.component';

// Service API
import {BaseService} from '../../services/common/base.service';
import {MediaService} from '../../services/media.service';
import {ReferenceService} from '../../services/reference.service';
import {ErrorService} from '../../services/common/error.service';
import {ContactService} from '../../services/contact.service';
import {ThemeService} from '../../services/theme.service';


// Contrib
import {SlickModule} from 'ngx-slick';
import {PdfViewerModule} from 'ng2-pdf-viewer';

// Scroll
import {ScrollService} from '../../services/scroll.service';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import {FlexLayoutModule} from '@angular/flex-layout';
import {BREAKPOINTS, DEFAULT_BREAKPOINTS} from '@angular/flex-layout';
const BreakPointsProvider = {
  provide: BREAKPOINTS,
  useValue: DEFAULT_BREAKPOINTS
};

// Material
import {
  MatOptionModule,
  MatSelectModule,
  MatCardModule,
  MatDividerModule,
  MatCheckboxModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatDialogModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatTreeModule,
  MatSnackBarModule,
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatProgressSpinnerModule,
} from '@angular/material';

@NgModule({
  imports: [
    FlexLayoutModule,
    OverlayModule,
    A11yModule,
    // Material
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatSelectModule,
    MatDividerModule,
    MatCheckboxModule,
    MatMenuModule,
    MatDialogModule,
    MatToolbarModule,
    MatTreeModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    // Contrib
    SlickModule,
    PdfViewerModule,
    PerfectScrollbarModule,
  ],
  declarations: [
    ForbiddenComponent,
    MaintenanceComponent,
    NotFoundComponent,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    // Contrib
    SlickModule,
    PdfViewerModule,
    PerfectScrollbarModule,

    FlexLayoutModule,
    OverlayModule,
    A11yModule,
    // Material
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    MatDividerModule,
    MatCheckboxModule,
    MatMenuModule,
    MatDialogModule,
    MatToolbarModule,
    MatTreeModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatButtonToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
  ],
  providers:[
    BaseService,
    MediaService,
    ErrorService,
    ReferenceService,
    ContactService,
    ThemeService,
    BreakPointsProvider,
    ScrollService,
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
  ]
})
export class SharedModule {
}
