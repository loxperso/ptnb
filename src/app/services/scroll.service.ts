import {Injectable} from '@angular/core';

// Rxjs
import { Observable } from 'rxjs';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


@Injectable()
export class ScrollService {

  private scrollEnd = new BehaviorSubject<any>(false);
  private isData = new BehaviorSubject<any>(null);
  private isHome = new BehaviorSubject<any>(false);

  constructor() {}

  public scroll() {
    return this.scrollEnd.asObservable();
  }

  public onReachEnd() {
    this.scrollEnd.next(true);
  }

  public positionFooterOnScrollHeight() {
    $('footer').removeClass('relative-bottom');
    const h0 = $('.ps-content').height();
    const h1 = $('.ps-content > .main-container.inner').height();
    if (h0 < h1 + 65) {
      $('footer').addClass('relative-bottom');
    }
    $('footer').addClass('display');
  }

  public positionFooterHome() {
    $('.ps-content > .main-container.inner').height('100%');
    const height_window = $(window).height();
    const  h0 = $('.ps-content').height();
    const h1 = $('.ps-content > .main-container.inner').height();
    setTimeout( () => {
      const height_dots_top = $('.slick-dots').offset().top + $('.slick-dots').height() + $('footer').height();
      if (height_dots_top > height_window) {
        $('.ps-content > .main-container.inner').height(h1 + 200);
      }
    }, 500);
  }

  public resetHeightFooter() {
      $('.ps-content > .main-container.inner').height('100%');
  }

  public setClickCloseTopbar (data: any) {
    this.isData.next(data);
  }

  public getClickCloseTopbar(): Observable<any> {
    return this.isData.asObservable();
  }

  public setIsHome(data: any) {
    this.isHome.next(data);
  }

  public getIsHome(): Observable<any> {
    return this.isHome.asObservable();
  }
}
