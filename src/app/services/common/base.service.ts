import {Injectable} from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {ErrorService} from './error.service';

import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {API_HOST, API_URL_PREFIX} from './constant.service';

@Injectable()
export class BaseService {
  protected headers: HttpHeaders;

  constructor(protected http: HttpClient,
              protected errorHandler: ErrorService) {
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Access-Control-Max-Age', '3600');
  }

  static createAPIURL(path: string): string {
    return API_HOST + API_URL_PREFIX + path;
  }

  static createCustomAPIURL(path: string): string {
    const customURL = '/' + API_URL_PREFIX.split('/')[1] + '/';
    return API_HOST + customURL + path;
  }

  public get(url: string, params: HttpParams) {
    return this.http.get(url, {headers: this.headers, params: params})
      .catch(error => this.errorHandler.handleError(error));
  }


  public post(url: string, params: HttpParams) {
    return this.http.post(url, params, {headers: this.headers})
      .catch(error => this.errorHandler.handleError(error));
  }

  public put(url: string, params: HttpParams) {
    return this.http.put(url, params, {headers: this.headers})
      .catch(error => this.errorHandler.handleError(error));
  }

  public remove(url: string, params: HttpParams) {
    return this.http.delete(url, {headers: this.headers, params: params})
      .catch(error => this.errorHandler.handleError(error));
  }
}
