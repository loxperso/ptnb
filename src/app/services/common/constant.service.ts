import {environment} from '../../../environments/environment';

export const API_HOST = environment.apiHost;
export const API_URL_PREFIX = environment.apiUrlPrefix;
