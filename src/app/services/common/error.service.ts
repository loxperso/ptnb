import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

import {throwError} from 'rxjs';

/**
 * Default error handler
 */
@Injectable()
export class ErrorService {

  constructor(private router: Router) {
  }

  /**
   * Handle error function
   * @param {Response} error
   * @return {any}
   */
  public handleError(error: HttpErrorResponse): any {
    return throwError(error)
  }
}
