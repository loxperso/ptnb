import {Injectable} from '@angular/core';

import {HttpClient, HttpParams} from '@angular/common/http';
import {ErrorService} from './common/error.service';
import {BaseService} from './common/base.service';

@Injectable()
export class LoginService extends BaseService {

  private BASE_PATH = 'login';

  constructor(protected http: HttpClient,
              protected errorHandler: ErrorService) {
    super(http, errorHandler);
  }

  public postLogin(params?: HttpParams) {
    const _url: string = BaseService.createCustomAPIURL(this.BASE_PATH);
    return this.post(_url, params);
  }

  //faire une fonction load

}
