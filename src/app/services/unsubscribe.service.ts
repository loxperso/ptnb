import {Injectable} from '@angular/core';

import {HttpClient, HttpParams} from '@angular/common/http';
import {ErrorService} from './common/error.service';
import {BaseService} from './common/base.service';

@Injectable()
export class UnsubscribeService extends BaseService {

  private BASE_PATH = 'unsubscribe';

  constructor(protected http: HttpClient,
              protected errorHandler: ErrorService) {
    super(http, errorHandler);
  }

  public postUnsubscribe(params?: HttpParams) {
    const _url: string = BaseService.createAPIURL(this.BASE_PATH);
    return this.post(_url, params);
  }

}
