import {Injectable} from '@angular/core';

import {HttpClient, HttpParams} from '@angular/common/http';
import {ErrorService} from './common/error.service';
import {BaseService} from './common/base.service';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class MediaService extends BaseService {

  private BASE_PATH = 'medias';
  private MEDIA_LATEST = '/latest';
  private dialogIsloseInstrance = new BehaviorSubject<any>(null);

  constructor(protected http: HttpClient,
              protected errorHandler: ErrorService) {
    super(http, errorHandler);
  }

  public getList(params?: HttpParams) {
    const _url: string = BaseService.createAPIURL(this.BASE_PATH);
    return this.get(_url, params);
  }

  public getById(id, params?: HttpParams) {
    const _url: string = BaseService.createAPIURL(`${this.BASE_PATH}/${id}`);
    return this.get(_url, params);
  }

  public getDownloadURL(id, params?: HttpParams) {
    const _url: string = BaseService.createAPIURL(`${this.BASE_PATH}/${id}/download`);
    return _url;
  }

  public getLatest(params?: HttpParams) {
    const _url: string = BaseService.createAPIURL(`${this.BASE_PATH}${this.MEDIA_LATEST}`);
    return this.get(_url, params);
  }
}
