import {Injectable} from '@angular/core';

import { Observable } from 'rxjs';

import {HttpClient, HttpParams} from '@angular/common/http';
import {ErrorService} from './common/error.service';
import {BaseService} from './common/base.service';

@Injectable()
export class ReferenceService extends BaseService {

  private _hasInit = false;
  private _data = {};
  private _queue = [];

  private BASE_PATH = 'reference_data';

  constructor(protected http: HttpClient,
              protected errorHandler: ErrorService) {
    super(http, errorHandler);
  }

  public init(cb?) {
    if(!this._hasInit) {
      // Avoid simultaneous requests
      this._hasInit = true;

      this.getList().map((data: any) => data.data).subscribe(data => {
        let professions = data.filter(v => v.type === 'professions') || [];
        let themes = data.filter(v => v.type === 'themes') || [];
        let messages = data.filter(v => v.type === 'messages') || [];

        this.setData('professions', data.filter(v => v.type === 'professions') || []);
        this.setData('themes', data.filter(v => v.type === 'themes') || []);
        this.setData('types', data.filter(v => v.type === 'types') || []);
        this.setData('messages', data.filter(v => v.type === 'messages') || []);

        if(cb) {
          cb();
        }

        this._execQueue();
      });
    } else {
      this._addToQueue(cb);
    }
  }

  public getList(params: HttpParams = new HttpParams()) {
    const _url: string = BaseService.createAPIURL(this.BASE_PATH);
    return this.get(_url, params);
  }

  private setData(key, res) {
    this._data[key] = res;
  }

  public getData(key, id = null) {
    const me = this;
    return Observable.create(function(observer) {
      if(!me._data[key]) {
        me.init(() => observer.next(id ? me._data[key].filter(d => d.id === id)[0] : me._data[key]))
      } else {
        observer.next(id ? me._data[key].filter(d => d.id === id)[0] : me._data[key]);
      }
    });
  }

  private _execQueue() {
    this._queue.map(m => m());
  }

  private _addToQueue(cb) {
    this._queue.push(cb);
  }

}
