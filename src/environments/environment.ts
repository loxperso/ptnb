export const environment = {
  production: false,
  apiHost: 'https://citae-ptnb.app.siclo-mobile.com',
  apiUrlPrefix: '/api/public/'
};
