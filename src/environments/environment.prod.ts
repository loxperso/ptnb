export const environment = {
  production: true,
  apiHost: 'https://citae-ptnb.app.siclo-mobile.com',
  apiUrlPrefix: '/api/public/'
};
